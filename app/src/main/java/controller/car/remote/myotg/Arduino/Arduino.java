package controller.car.remote.myotg.Arduino;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;

import controller.car.remote.myotg.Arduino.enums.ArduinoReturnValues;
import controller.car.remote.myotg.Arduino.model.UsbArduinoInterface;
import controller.car.remote.myotg.MainActivity;
import controller.car.remote.myotg.Utils.UserNotifier;

/**
 * Created by root on 1/27/18.
 */

public class Arduino implements Runnable {

    private MainActivity mainActivity;
    private UsbManager usbManager;
    private UsbDevice usbDevice;
    private UsbArduinoInterface usbArduinoInterface;
    private UsbDeviceConnection connection;
    private PendingIntent mPermissionIntent;
    private static final String ACTION_USB_PERMISSION = "controller.car.remote.myotg.USB_PERMISSION";
    private static final byte IGNORE_00 = (byte) 0x00;
    private String stringToRx = "";
    private boolean isConnected;
    //private static final byte IGNORE_00 = (byte) 0x00;
    private static final byte SYNC_WORD = (byte) 0xFF;

    private static final int vendorID = 6790,
                             productID = 29987;


    public Arduino(MainActivity mainActivity, UsbManager usbManager) {
        this.mainActivity = mainActivity;
        this.usbManager = usbManager;
    }

    public ArduinoReturnValues connect() {
        this.usbDevice = this.getUSBDevice();
        if(this.usbDevice == null) {
            UserNotifier.Error(this.mainActivity, "Couldn't find any USB Arduino device");
            return ArduinoReturnValues.NO_USB_FOUND;
        }

        this.usbArduinoInterface = this.getUsbArduinoInterface(this.usbDevice);
        if(this.usbArduinoInterface == null) {
            UserNotifier.Error(this.mainActivity, "Trying to retrieve the USB Interface");
            return ArduinoReturnValues.NO_USB_INTERFACE;
        }

        this.connection = this.setupConnection(this.usbDevice, this.usbArduinoInterface);
        if(this.connection == null) {
            UserNotifier.Error(this.mainActivity, "Couldn't setup a connection with a Arduino device");
            return ArduinoReturnValues.COULDNT_SETUP_CONNECTION;
        }

        this.isConnected = true;
        return ArduinoReturnValues.SUCCESS;
    }

    public void close() {
        if(this.connection != null)
            this.connection.close();

        if(this.usbArduinoInterface != null) {
            this.usbArduinoInterface.close();
            this.usbArduinoInterface = null;
        }

        if(this.usbDevice != null)
            this.usbDevice = null;

        this.mainActivity.unregisterReceiver(this.mUsbReceiver);
        this.isConnected = false;
    }

    public void sendCommand(int command) {
        synchronized (this) {

            if (this.connection != null) {
                byte[] message = new byte[1];
                message[0] = (byte)command;

                this.connection.bulkTransfer(this.usbArduinoInterface.getGetUsbEndpoint_out(), message, message.length, 0);
            }
        }
    }

    private UsbDevice getUSBDevice() {
        /*
		 * this block required if you need to communicate to USB devices it's
		 * take permission to usbDevice
		 * if you want than you can set this to which usbDevice you want to communicate
		 */
        // ------------------------------------------------------------------
        mPermissionIntent = PendingIntent.getBroadcast(this.mainActivity, 0, new Intent(
                ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        try {
            this.mainActivity.registerReceiver(mUsbReceiver, filter);
        } catch (Exception e) {}
        // -------------------------------------------------------------------
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        while (deviceIterator.hasNext()) {
            UsbDevice usbDevice = deviceIterator.next();
            //UserNotifier.Info(mainActivity, "VId: " + usbDevice.getVendorId() + " PId: " + usbDevice.getProductId());
            if (this.isArduinoDevice(usbDevice.getVendorId(), usbDevice.getProductId())) {
                usbManager.requestPermission(usbDevice, mPermissionIntent);
                return usbDevice;
            }
        }
        return null;
    }

    private UsbArduinoInterface getUsbArduinoInterface(UsbDevice usbDevice) {
        // Search for UsbInterface with Endpoint of USB_ENDPOINT_XFER_BULK,
        // and direction USB_DIR_OUT and USB_DIR_IN

        for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
            UsbInterface usbif = usbDevice.getInterface(i);

            UsbEndpoint tOut = null;
            UsbEndpoint tIn = null;

            int tEndpointCnt = usbif.getEndpointCount();
            if (tEndpointCnt >= 2) {
                for (int j = 0; j < tEndpointCnt; j++) {
                    if (usbif.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                        if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_OUT) {
                            tOut = usbif.getEndpoint(j);
                        } else if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_IN) {
                            tIn = usbif.getEndpoint(j);
                        }
                    }
                }

                if (tOut != null && tIn != null) {
                    // This interface have both USB_DIR_OUT
                    // and USB_DIR_IN of USB_ENDPOINT_XFER_BULK
                    return new UsbArduinoInterface(usbif, tIn, tOut);
                }
            }
        }

        return null;
    }

    private UsbDeviceConnection setupConnection(UsbDevice usbDevice, UsbArduinoInterface usbArduinoInterface) {

        UsbDeviceConnection connection = usbManager.openDevice(usbDevice);
        if (connection != null && connection.claimInterface(usbArduinoInterface.getUsbInterface(), true)) {

            connection.controlTransfer(0x21, 34, 0, 0, null, 0, 0);
            connection.controlTransfer(0x21, 32, 0, 0, new byte[] { (byte) 0x80, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08 }, 7, 0); //Baud rate of 9600
            return connection;
        }

        return null;
    }



    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(
                            UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            // call method to set up usbDevice communication

                        }
                    } else {
                        UserNotifier.Error(mainActivity, "Permission denied for USB device " + device);
                    }
                }
            }
        }
    };

    public boolean isArduinoDevice(int vendorID, int productID) {
        return (Arduino.vendorID == vendorID && productID == Arduino.productID);
    }

    public boolean isConnectedToArduino(){
        return this.isConnected;
    }

    @Override
    public void run() {
        ByteBuffer buffer = ByteBuffer.allocate(1);
        UsbRequest request = new UsbRequest();
        request.initialize(this.connection, this.usbArduinoInterface.getUsbEndpoint_in());
        while (true) {
            request.queue(buffer, 1);
            if (this.connection.requestWait() == request) {
                byte dataRx = buffer.get(0);
                if(dataRx != IGNORE_00){
                    stringToRx += (char)dataRx;
                    this.mainActivity.runOnUiThread(new Runnable(){
                        @Override
                        public void run() {
                            mainActivity.getTextInfo().setText(stringToRx);
                        }});
                }
            } else {
                UserNotifier.Info(mainActivity, "It ended reading");
                break;
            }
        }
    }
}
