package controller.car.remote.myotg.Arduino.enums;

/**
 * Created by root on 1/27/18.
 */

public enum ArduinoReturnValues {
    SUCCESS, NO_USB_FOUND, NO_USB_INTERFACE, COULDNT_SETUP_CONNECTION, ALREADY_LISTENING, LISTENING
}
