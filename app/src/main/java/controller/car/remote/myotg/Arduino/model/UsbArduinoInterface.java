package controller.car.remote.myotg.Arduino.model;

import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;

/**
 * Created by root on 1/27/18.
 */

public class UsbArduinoInterface {
    private UsbInterface usbInterface;
    private UsbEndpoint usbEndpoint_in;
    private UsbEndpoint usbEndpoint_out;

    public UsbArduinoInterface(UsbInterface usbInterface, UsbEndpoint usbEndpoint_in, UsbEndpoint usbEndpoint_out) {
        this.usbInterface = usbInterface;
        this.usbEndpoint_in = usbEndpoint_in;
        this.usbEndpoint_out = usbEndpoint_out;
    }

    public void close() {
        this.usbEndpoint_in = null;
        this.usbEndpoint_out = null;
        this.usbInterface = null;
    }

    public UsbInterface getUsbInterface() {
        return this.usbInterface;
    }

    public UsbEndpoint getUsbEndpoint_in() {
        return this.usbEndpoint_in;
    }

    public UsbEndpoint getGetUsbEndpoint_out() {
        return this.usbEndpoint_out;
    }
}
