package controller.car.remote.myotg;

import android.hardware.Camera;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.ros.android.AppCompatRosActivity;
import org.ros.android.view.camera.RosCameraPreviewView;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.io.IOException;
import java.net.InetAddress;

import controller.car.remote.myotg.Utils.OnDriverCommandsMessage;
import controller.car.remote.myotg.Utils.UserNotifier;

public class MainActivity extends AppCompatRosActivity {

    private InetAddress localInetAddress;
    private RosCameraPreviewView rosCameraPreviewView;
    //private DriverNode driverNode;
    private Socket socketm;
    private OnDriverCommandsMessage ondrivercommandsmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.rosCameraPreviewView = findViewById(R.id.ros_camera_preview_view);
        this.setUpSocket();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setUpCamera();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        //this.driverNode.close();
        this.rosCameraPreviewView.releaseCamera();
        if(this.socketm != null) {
            this.socketm.disconnect();
            this.socketm.off();
            this.ondrivercommandsmsg.close();
        }
    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {
        java.net.InetAddress localNetworkAddress = this.getLocalNetworkAddress();
        if(localNetworkAddress != null) {
            NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(localNetworkAddress.getHostAddress(), this.getMasterUri());
            nodeMainExecutor.execute(this.rosCameraPreviewView, nodeConfiguration);
            //DriverNode driver = new DriverNode(this);
            //nodeMainExecutor.execute(driver, nodeConfiguration);
            this.setUpSocket();
        }
    }

    private java.net.InetAddress getLocalNetworkAddress() {
        if(this.localInetAddress != null)
            return this.localInetAddress;

        try {
            java.net.Socket socket = new java.net.Socket(this.getMasterUri().getHost(), this.getMasterUri().getPort());
            this.localInetAddress = socket.getLocalAddress();
            socket.close();
            return localInetAddress;
        }
        catch(IOException e) {
            UserNotifier.Error(this, "An error happen when trying to make a socket connection to the MasterClient");
        }

        return null;
    }

    private void setUpCamera() {
        this.rosCameraPreviewView.setCamera(Camera.open(0));
    }

    private void setUpSocket() {
        if(this.nodeMainExecutorService == null)
            return;

        if(this.socketm == null) {
            String url = "http://" + this.getMasterUri().getHost() + ":5000/car";
            try {
                this.socketm = IO.socket(url);
            } catch (final Exception e) {
                UserNotifier.Error(this, e.getMessage() + " || " + url);
            }
        }
        final MainActivity self = this;
        if(!this.socketm.hasListeners(Socket.EVENT_CONNECT)) {
            this.socketm.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    UserNotifier.Toast(self, "Connected to websocket", Toast.LENGTH_SHORT);
                }
            });
        }
        if(!this.socketm.hasListeners(Socket.EVENT_CONNECT_ERROR)) {
            this.socketm.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    UserNotifier.Error(self, "Couldn't connect to websocket");
                }
            });
        }
        if(!this.socketm.hasListeners(getString(R.string.DRIVER_DISCONNECTED))) {
            this.socketm.on(getString(R.string.DRIVER_DISCONNECTED), new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    self.ondrivercommandsmsg.stopCar();
                }
            });
        }
        if(!this.socketm.hasListeners(getString(R.string.DRIVER_CMD)))
            this.ondrivercommandsmsg = new OnDriverCommandsMessage(this);
            this.socketm.on(getString(R.string.DRIVER_CMD), this.ondrivercommandsmsg);
        if(!this.socketm.connected())
            this.socketm.connect();
    }

    public TextView getTextInfo() {
        return null;
    }
}


