package controller.car.remote.myotg.Nodes;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.widget.Toast;

import org.apache.commons.logging.Log;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.topic.Subscriber;

import controller.car.remote.myotg.Arduino.Arduino;
import controller.car.remote.myotg.Arduino.enums.ArduinoReturnValues;
import controller.car.remote.myotg.MainActivity;
import controller.car.remote.myotg.Utils.UserNotifier;

/**
 * A simple {@link Subscriber} {@link NodeMain}.
 *
 * @author damonkohler@google.com (Damon Kohler)
 */
public class DriverNode extends AbstractNodeMain implements NodeMain {

    private Arduino arduino;
    private MainActivity mainActivity;

    public DriverNode(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        this.arduino = new Arduino(mainActivity, (UsbManager) this.mainActivity.getSystemService(Context.USB_SERVICE));
        if(!arduino.isConnectedToArduino()) {
            this.connectToArduino();
        }
    }

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("driver_commands");
    }

    @Override
    public void onStart(ConnectedNode connectedNode) {
        final Log log = connectedNode.getLog();
        Subscriber<std_msgs.Int8> subscriber = connectedNode.newSubscriber("/remote/car/driver", std_msgs.Int8._TYPE);
        subscriber.addMessageListener(new MessageListener<std_msgs.Int8>() {
            @Override
            public void onNewMessage(std_msgs.Int8 message) {
                log.info("I heard: \"" + message.getData() + "\"");
                Toast.makeText(mainActivity, "msg: " + message.getData(), Toast.LENGTH_LONG).show();
                if(!arduino.isConnectedToArduino()) {
                    UserNotifier.Error(mainActivity, "[NOT CONNECTED] Couldn't deliver '" + message.getData() + "' to Arduino");
                    return;
                }
                arduino.sendCommand(message.getData());
            }
        });
    }

    public void onShutdown(Node node) {

        UserNotifier.Info(mainActivity, "onShutdown");
    }

    public void onShutdownComplete(Node node) {
    }

    public void onError(Node node, Throwable throwable) {
        UserNotifier.Error(mainActivity, "OnError");
    }

    private boolean connectToArduino() {
        ArduinoReturnValues result = this.arduino.connect();
        if(result == ArduinoReturnValues.SUCCESS) {
            Toast.makeText(mainActivity, "Connected to Arduino with success", Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    public void close() {
        this.arduino.close();
    }
}
