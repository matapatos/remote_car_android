package controller.car.remote.myotg.Utils;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.widget.Toast;


import com.github.nkzawa.emitter.Emitter;

import controller.car.remote.myotg.Arduino.Arduino;
import controller.car.remote.myotg.Arduino.enums.ArduinoReturnValues;
import controller.car.remote.myotg.MainActivity;

public class OnDriverCommandsMessage implements Emitter.Listener {

    private Arduino arduino;
    private MainActivity mainActivity;

    public OnDriverCommandsMessage(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.arduino = new Arduino(mainActivity, (UsbManager) this.mainActivity.getSystemService(Context.USB_SERVICE));
        if(!arduino.isConnectedToArduino()) {
            this.connectToArduino();
        }
    }

    private boolean connectToArduino() {
        ArduinoReturnValues result = this.arduino.connect();
        if(result == ArduinoReturnValues.SUCCESS) {
            UserNotifier.Toast(this.mainActivity, "Connected to Arduino with success", Toast.LENGTH_SHORT);
            return true;
        }
        return false;
    }

    public void close() {
        this.arduino.close();
    }

    public void stopCar() {
        this.arduino.sendCommand(9);
        UserNotifier.Info(this.mainActivity, "Stop car");
    }

    @Override
    public void call(Object... args) {
        if(args.length == 0) {
            UserNotifier.Info(this.mainActivity, "Message from driver is empty");
        }
        else {
            if(!this.arduino.isConnectedToArduino()) {  // Make sure that is connected to Arduino
                if(!this.connectToArduino()) {
                    return;
                }
            }
            int command = (int)args[0];
            this.arduino.sendCommand(command);
        }
    }
}

