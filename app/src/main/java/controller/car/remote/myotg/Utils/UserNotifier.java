package controller.car.remote.myotg.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import controller.car.remote.myotg.MainActivity;

/**
 * Created by root on 1/27/18.
 */

public class UserNotifier {
    public static void Error(Activity activity, String message) {
        UserNotifier.Alert(activity, "Error", message);
    }

    public static void Info(Activity activity, String message) {
        UserNotifier.Alert(activity, "Info", message);
    }

    public static void Toast(final Activity activity, final String message, final int length) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, length).show();
            }
        });
    }

    private static void Alert(final Activity activity, final String title, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(message)
                        .setCancelable(true)
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                if(dialogInterface != null)
                                    dialogInterface.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }
}
